package com.kolchak.task_1;

@FunctionalInterface
public interface FuncInterf {
    int calculate(int a, int b, int c);
}
