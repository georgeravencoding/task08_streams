package com.kolchak.task_3;

import java.util.ArrayList;
import java.util.List;

public class ListGenerator {

    public static List<Integer> firstGenerator() {
        List<Integer> integerList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            integerList.add((int) (Math.random()*100));
        }
        return integerList;
    }

    public static List<Integer> secondGenerator() {
        List<Integer> integerList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            integerList.add((int) (Math.random()*100));
        }
        return integerList;
    }

    public static List<Integer> thirdGenerator() {
        List<Integer> integerList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            integerList.add((int) (Math.random()*100));
        }
        return integerList;
    }
}
