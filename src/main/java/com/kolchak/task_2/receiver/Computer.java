package com.kolchak.task_2.receiver;

public class Computer {
    private static String computerOn = "Computer on";
    private static String computerOff = "Computer off";

    public Computer() {
    }

    public static String turnOn() {
        return computerOn;
    }

    public static String turnOff() {
        return computerOff;
    }
}
