package com.kolchak.task_2.command;

@FunctionalInterface
public interface Power {
    String powerSwitch();
}
