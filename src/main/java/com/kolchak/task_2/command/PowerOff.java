package com.kolchak.task_2.command;

import com.kolchak.task_2.receiver.Computer;

public class PowerOff implements Power {
    private Computer computer;

    public PowerOff(String computer) {
    }

    public String powerSwitch() {
        return computer.turnOff();
    }
}
