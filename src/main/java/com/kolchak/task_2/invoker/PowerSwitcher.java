package com.kolchak.task_2.invoker;

import com.kolchak.task_2.command.Power;

import java.util.ArrayList;
import java.util.List;

public class PowerSwitcher {
    public final List<Power> powerList = new ArrayList<>();

    public String switcher(Power power) {
        powerList.add(power);
        return power.powerSwitch();
    }

}
