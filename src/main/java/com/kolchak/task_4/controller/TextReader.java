package com.kolchak.task_4.controller;

import com.kolchak.task_4.model.TextContainer;
import org.apache.logging.log4j.core.appender.AbstractOutputStreamAppender;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TextReader {
    TextContainer textContainer = new TextContainer();

    FileReader fileReader;
    BufferedReader bufferedReader;

    {
        try {
            fileReader = new FileReader(textContainer.getFile());
            bufferedReader = new BufferedReader(fileReader);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void readFile() throws IOException {
        while ((textContainer.line = bufferedReader.readLine()) != null) {
            System.out.println(textContainer.line);
        }
    }
}
