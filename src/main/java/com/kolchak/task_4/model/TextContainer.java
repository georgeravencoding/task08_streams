package com.kolchak.task_4.model;

import java.io.*;

public class TextContainer {
    private String file = "someText.txt";
    public String line = null;

    public TextContainer() {
    }

    public TextContainer(String file, String line) {
        this.file = file;
        this.line = line;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    @Override
    public String toString() {
        return "TextReader{" +
                "file='" + file + '\'' +
                ", line='" + line + '\'' +
                '}';
    }
}
