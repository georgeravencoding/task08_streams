package com.kolchak;

import com.kolchak.task_1.FuncInterf;
import com.kolchak.task_2.invoker.PowerSwitcher;
import com.kolchak.task_2.receiver.Computer;
import com.kolchak.task_3.ListGenerator;
import com.kolchak.task_4.controller.TextReader;
import org.apache.logging.log4j.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Application {
    public static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {

//----------------------------Task 1-------------------------------

        FuncInterf maxValue = (a, b, c) -> {
            if (a > b && a > c) {
                return a;
            } else if (b > c) {
                return b;
            } else return c;
        };
        FuncInterf average = (a, b, c) -> (a + b + c) / 3;

        logger.info(maxValue.calculate(20, 40, 50));
        logger.info(average.calculate(20, 40, 60));

//----------------------------Task 2-------------------------------

        PowerSwitcher powerSwitcher = new PowerSwitcher();

        String switchPowerToOn = powerSwitcher.switcher(Computer::turnOn);
        String switchPowerToOff = powerSwitcher.switcher(() -> Computer.turnOff());

        logger.info(switchPowerToOn);
        logger.info(switchPowerToOff);

//----------------------------Task 3-------------------------------

        List<List<Integer>> listGenerators = new ArrayList<>();
        listGenerators.add(ListGenerator.firstGenerator());
        listGenerators.add(ListGenerator.secondGenerator());
        listGenerators.add(ListGenerator.thirdGenerator());


        List<Integer> integerList = listGenerators.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        Optional<Integer> max = integerList.stream()
                .max(Integer::compareTo);

        Optional<Integer> min = integerList.stream()
                .min(Integer::compareTo);

        Optional<Integer> sum1 = integerList.stream()
                .reduce((integer, integer2) -> integer + integer2);

        Optional<Integer> sum2 = integerList.stream()
                .reduce(Integer::sum);

        logger.info("Integer list: " + integerList);
        logger.info("Max value: " + max + ", min value: " + min + ", summary: " + sum1 + " or " + sum2);

//----------------------------Task 4-------------------------------

        TextReader textReader = new TextReader();
        try {
            textReader.readFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}


